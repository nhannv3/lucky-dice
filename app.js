$(document).ready(function(){
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gDataInp = {}
    const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
    const gREQUEST_STATUS_OK = 200;
    const gBASE_URL = "http://42.115.221.44:8080/devcamp-lucky-dice/";
    const gUTF8_TEXT_APPLICATION_HEADER =  "application/json;charset=UTF-8";
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    $("#btn-dice").click(function(){
        onBtnDiceClick();
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onBtnDiceClick() {
        // thu thập dữ liệu
        getDataInp();
        // validate
        var isValidate = checkDataInp();
        if(isValidate) {
            // show data
            showDataInp()
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-lucky-dice/" + "/dice",
                type: 'post',
                dataType: 'json',
                data: JSON.stringify(gDataInp),
                contentType:"Content-Type",
                contentType:"application/json;charset=UTF-8",
                processData: true,
                success: function (data) {
                    console.log(data);
                    showNewDice(data);
                },
            });
        }
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    function getDataInp() {
        gDataInp.username = $("#inp-user-name").val().trim();
        gDataInp.fname = $("#inp-first-name").val().trim();
        gDataInp.lname = $("#inp-last-name").val().trim();
    }
    function checkDataInp() {
        if(gDataInp.username === "") {
            alert("chưa nhập username");
            return false;
        }
        if(gDataInp.fname === "") {
            alert("chưa nhập firstname");
            return false;
        }
        if(gDataInp.lname === "") {
            alert("chưa nhập lastname");
            return false;
        }
        return true
    }
    function showDataInp() {
        console.log("First Name: ", gDataInp.fname);
        console.log("Last Name: ", gDataInp.lname);
        console.log("User Name: ", gDataInp.username);
    }
    function showNewDice(paramResponse) {
        var vMessage = $("#p-notification-dice");
        var vVoucher =  $("#p-voucher-id");
        var vDiscount = $("#p-voucher-percent");
        var vPresent = $("#img-present");
        $("#img-dice").prop("src",`LuckyDiceImages/${paramResponse.dice}.png`);
        if(paramResponse.dice < 3) 
            vMessage.html("Chúc may mắn lần sau");
        else if(paramResponse.dice < 5)
            vMessage.html("Gần được rồi hãy cố gắn lên");
        else
            vMessage.html("Thành công");
        if(paramResponse.voucher  === null)
            vVoucher.html(`No ID`);
        else
            vVoucher.html(`ID: ${paramResponse.voucher.maVoucher}`);
        if(paramResponse.voucher  === null) 
            vDiscount.html(`0%`);
        else
            vDiscount.html(`${paramResponse.voucher.phanTramGiamGia}%`);
        if(paramResponse.prize === null)
            vPresent.attr("src","LuckyDiceImages/no-present.jpg")
        else if(paramResponse.prize === "Xe máy")
            vPresent.attr("src","LuckyDiceImages/motobike.jpg")
        else if(paramResponse.prize === "Mũ")
            vPresent.attr("src","LuckyDiceImages/hat.jpg")
        else if(paramResponse.prize === "Áo")
            vPresent.attr("src","LuckyDiceImages/t-shirt.jpg")
        else
            vPresent.attr("src","LuckyDiceImages/car.jpg")
    }
});